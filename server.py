from flask import Flask ,render_template,request,json,jsonify
import numpy as np
import csv
from scipy.spatial import distance
from sklearn import preprocessing,datasets

from sklearn.neighbors import NearestNeighbors
from sklearn.datasets import load_diabetes, load_linnerud
from python.mapper import *
from python.tool import *

from python.data import getCyclus_Power,getNetworkData

app = Flask(__name__)
######################################################################
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/mapper')
def mapper():
    return render_template('mapper.html')

@app.route('/dpc')
def dpc():
    return render_template('densityParallelCoordinate.html')

@app.route('/matrix')
def matrix():
    return render_template('graph.html')
######################################################################
@app.route('/_getData', methods=['GET', 'POST'])
def getData():
    json_request = request.get_json()
    dim        = json_request['dim']
    filter     = json_request['filter']
    clustering = json_request['clustering']
    cluster_n  = json_request['cluster_n']
    cube_n     = json_request['cube_n']
    #metrics    = json_request['metrics']
    overlap    = json_request['overlap']

    digits = datasets.load_digits()
    data    = digits.data
    target  = digits.target

    clustermethod  = Clustering(clustering, cluster_n)
    mapper = Mapper()

    if dim == '1D':
        projected_data = Embedding(filter, data, 1)
        result = mapper.Mapper_1D(projected_data, data, clustermethod, n = int(cube_n), overlap=float(overlap))
    else:
        projected_data = Embedding(filter, data, 2)
        result = mapper.Mapper_2D(projected_data, data, clustermethod, n = int(cube_n), overlap=float(overlap))
    
    #convert the index to ground true label.
    #index = np.argsort(projected_data)
    #reorder_data = []
    #grandtrue = []
    #for t in index:
    #    grandtrue.append(target[t])
    #    reorder_data.append(data[t].tolist())
    result['target'] = target.tolist()
    result['data'] = data.tolist()
    result['dim_reducton'] = projected_data.tolist()
    return json.dumps(result)
######################################################################
@app.route('/_getGraphData', methods=['GET', 'POST'])
def _getFileData():
    json_request = request.get_json()
    file        = json_request['file']

    result = getNetworkData()
    return json.dumps(result)
if __name__ == '__main__':
    app.run(debug=True)