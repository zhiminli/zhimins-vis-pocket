graph
[
  directed 0
  node
  [
    id 0
    label "a"
  ]
  node
  [
    id 1
    label "b"
  ]
  node
  [
    id 2
    label "c"
  ]
  node
  [
    id 3
    label "d" 
  ]
  node
  [
    id 4
    label "e"
  ]
  node
  [
    id 5
    label "f"
  ]
  node
  [
    id 6
    label "g"
  ] 
  node
  [
    id 7
    label "h"
  ]
  node
  [
    id 8
    label "i"
  ]
  node
  [
    id 9
    label "j"
  ]
  node
  [
    id 10
    label "k"
  ]
  node
  [
    id 11
    label "l"
  ]
  edge
  [
    source 1
    target 5
  ]
  edge
  [
    source 2
    target 3
  ]
  edge
  [
    source 1
    target 3
  ] 
  edge
  [
    source 1
    target 8
  ]
  edge
  [
    source 2
    target 6
  ]
  edge
  [
    source 8
    target 11
  ]
  edge
  [
    source 7
    target 9
  ]
  edge
  [
    source 10
    target 2
  ]
  edge
  [
    source 3
    target 8
  ]
]