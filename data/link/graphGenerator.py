import networkx as nx

graph_size = 100
density = 0.2



G = nx.fast_gnp_random_graph(graph_size, density)
for i in range(graph_size/2):
    G.add_edge(50, i+1) 

nx.write_gml(G, "G1_" + str(graph_size) + "_" + str(density) + ".gml")


