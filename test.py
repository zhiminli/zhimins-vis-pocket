from python.mapper import *
from sklearn import datasets, cluster
from sklearn.decomposition import PCA


data = datasets.load_digits().data
pca = PCA(n_components=2)
projected_data = pca.fit_transform(data)
#projected_data = projected_data.ravel()

mapper = Mapper()

#print mapper.Mapper_1D(projected_data, data, cluster.DBSCAN(eps=0.5, min_samples=2, metric='euclidean'), n=10, overlap=0.7)
print mapper.Mapper_1D(projected_data, data, cluster.AgglomerativeClustering(n_clusters=int(2), affinity='euclidean'), n=10, overlap=0.5)