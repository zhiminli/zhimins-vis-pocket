import numpy as np
import networkx as nx

def getCyclus_Power():
    with open('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/error.csv') as f:
        lines = f.readlines()
    
    attribute = lines.pop(0).strip('\r\t\n ').split(',')
    data = []
    for line in lines:
        data.append(np.array(line.strip('\r\n\t ').split(','), dtype='float'))
    data = np.array(data)

    table = []
    for i in range(len(data[0])):
        table.append(data[:,i].tolist())
    
    return {'table':table, 'attribute':attribute}

def getNetworkData(filename='polbooks'):
    
    #G = nx.read_gml('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/G1_30_0.2.gml')

    #link 
    #G = nx.read_gml('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/matrix/G1_15_0.2.gml')
    #G = nx.read_gml('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/link/G1_30_0.2.gml')
    #G = nx.read_gml('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/link/G1_50_0.2.gml')
    #G = nx.read_gml('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/link/G1_100_0.2.gml')
    
    
    #matrix 
    #G = nx.read_gml('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/matrix/G1_15_0.2.gml')
    #G = nx.read_gml('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/matrix/G1_30_0.2.gml')
    #G = nx.read_gml('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/matrix/G1_50_0.2.gml')
    #G = nx.read_gml('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/matrix/G1_100_0.2.gml')
    
    #graph 
    #G = nx.read_gml('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/graph/G1_15_0.2.gml')
    #G = nx.read_gml('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/graph/G1_30_0.2.gml')
    #G = nx.read_gml('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/graph/G1_50_0.2.gml')
    #G = nx.read_gml('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/graph/G1_100_0.2.gml')
   
    

    G = nx.read_gml('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/polbooks.gml')


    


    
    
    

   

    

    

    

    

    

    

    #random network
    #G=nx.newman_watts_strogatz_graph(50, 4, 0.1)
    #G=nx.fast_gnp_random_graph(100, 0.2)
    #G = nx.wheel_graph(10) 
    keys = G.node.keys()


    labels = {}

    dependency = {}
    for key in keys:
        dependency[key] = G.edge[key].keys()
        labels[key] = str(chr(ord("A")+key%26)) + str(key/26)#G.node[key]['label']
    
    matrix = np.zeros((len(keys), len(keys)))
    for key in keys:
        for node in G.edge[key].keys():
            matrix[key][node] = 1;
        
    return {'keys':keys, 'dependency':dependency, 'matrix':matrix.ravel().tolist(), 'label':labels}

    



