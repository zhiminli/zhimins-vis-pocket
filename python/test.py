import numpy as np
import networkx as nx

G = nx.read_gml('/Users/lizhimin/Desktop/zhiMin_s_Vis_Pocket/data/dolphins.gml')
keys = G.node.keys()

dependency = {}
for key in keys:
    dependency[key] = G.edge[key].keys()

matrix = np.zeros((len(keys), len(keys)))
for key in keys:
    for node in G.edge[key].keys():
        matrix[key][node] = 1;