from sklearn.decomposition import PCA
from sklearn.manifold  import MDS, TSNE, SpectralEmbedding
from sklearn.cluster import KMeans, AgglomerativeClustering, DBSCAN

import numpy as np

def Embedding(method, data, dimension):  
    if method  == 'PCA':
        embedding = PCA(n_components=dimension)
    elif method == 'MDS':
        embedding = MDS(n_components=dimension)
    elif method == 'TSNE':
        embedding = TSNE(n_components=dimension)
    elif method == 'SpectralEmbedding':
        embedding = SpectralEmbedding(n_components=dimension)
    elif method == 'Metric':
        projected_data = []
        for d in data:
            projected_data.append([np.linalg.norm(np.array(d), 1),np.linalg.norm(np.array(d))])
        return np.array(projected_data)


    projected_data = embedding.fit_transform(data)
    projected_data = projected_data
    return projected_data

def Clustering(method, n):
    #return AgglomerativeClustering(n_clusters=int(n), affinity='euclidean')
    if method == 'Hierarchical Clustering':
        return AgglomerativeClustering(n_clusters=int(n), affinity='euclidean')
    elif method == 'DBSCANE':
        return DBSCAN(eps=0.5, min_samples=2, metric='l1', algorithm='auto', leaf_size=30, p=None, n_jobs=1)
