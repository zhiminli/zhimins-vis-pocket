import numpy as np
import bisect

from sklearn import cluster, preprocessing, manifold

class Mapper(object):
    
    
    def __init__(self, verbose=2):
        pass
##############################################################################################################         
    def Mapper_1D(self, projection_X, X, clusterFunction, n, overlap=0.2):
        # projection_X: data projected into one dimensional space 
        # X: the original data
        # clusterFunction: the clustering method used to cluster the data in original high dimenisonal space.
        # n: the number of intervel
        # overlap: the percentage of overlap between each intervel
        projection_X = projection_X.ravel()
        link  = []
        data  = []
        nodes = {}
        
        Max = np.max(projection_X)
        Min = np.min(projection_X) 
        gap = (Max - Min)/(n - overlap * n + overlap)
        intervel = np.array([Min, Min + gap])
        intervel = np.array([Min, Min + gap])
        # construct clustering in each sub-cube

        for cube in range(n):
            intervel_data = []
            for index, d in enumerate(projection_X):
                if intervel[0] <= d <=  intervel[1]:
                    intervel_data.append([index, d])
            
            #if the element in the data set are smaller than the number of cluster
            if len(intervel_data) == 0:
                intervel = intervel + (1 - overlap) * gap 
                continue
            elif len(intervel_data) == 1:
                labels = np.array([0]) + len(nodes)
            else:    
                labels = clusterFunction.fit(np.array(intervel_data)[:, 1].reshape(-1,1)).labels_
                labels = np.array(labels) + len(nodes)

            for i, l in enumerate(labels):
                if l in nodes:
                    nodes[l].append(intervel_data[i][0])
                else:
                    nodes[l] = [intervel_data[i][0]]
            intervel = intervel + gap - gap * overlap

        #set up the link between each node
        for i in range(len(nodes)):
            for j in range(i+1, len(nodes)):
                if bool(set(nodes[i]) & set(nodes[j])):
                        link.append([i, j])    
        return {'link':link, 'node':nodes}

 
##############################################################################################################     
    def Mapper_2D(self, projection_X, X, clusterFunction, n, overlap=0.5):
        # projection_X: Data projected into one dimensional space 
        # X: The original data
        # clusterFunction: The clustering method used to cluster the data in original high dimenisonal space.
        # n: The number of intervel
        # overlap: the percentage of overlap between each intervel
        link  = []
        data  = []
        nodes = {}
        proj_data = []

        #boundary of the data
        X_min = np.min(projection_X[:, 0])
        X_max = np.max(projection_X[:, 0])
        Y_min = np.min(projection_X[:, 1])
        Y_max = np.max(projection_X[:, 1])

        #create rectangle
        gap_x = (X_max - X_min)/(n - overlap * n + overlap)
        gap_y = (Y_max - Y_min)/(n - overlap * n + overlap)
        intervel_y = np.array([Y_min, Y_min + gap_y])

        for i in range(n):
            intervel_x = np.array([X_min, X_min + gap_x])
            for j in range(n):
                intervel_data = []
                for i, d in enumerate(projection_X):
                    if intervel_x[0]<= d[0] <= intervel_x[1] and intervel_y[0]<= d[1] <= intervel_y[1]:
                        intervel_data.append(np.append(i, d))
                    
                if len(intervel_data) == 0:
                    intervel_x = intervel_x + (1 - overlap) * gap_x 
                    continue
                elif len(intervel_data) == 1:
                    labels = np.array([0]) + len(nodes)
                else:    
                    labels = clusterFunction.fit(np.array(intervel_data)[:,1:3]).labels_
                    labels = np.array(labels) + len(nodes)

                for i, l in enumerate(labels):
                    if l in nodes:
                        nodes[l].append(intervel_data[i][0])
                else:
                    nodes[l] = [intervel_data[i][0]]
                intervel_x = intervel_x + (1 - overlap) * gap_x 
            intervel_y = intervel_y + (1 - overlap) * gap_y 

        for i in range(len(nodes)):
            if i not in nodes:
                continue
            for j in range(i+1, len(nodes)):
                if j not in nodes:
                    continue
                if bool(set(nodes[i]) & set(nodes[j])):
                        link.append([i, j])    
        return {'link':link, 'node':nodes}

                

        