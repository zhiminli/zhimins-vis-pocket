/************************************************************************
************************************************************************
Density Parallel Coordinate:
    canvas,
    d3
************************************************************************
*************************************************************************/
//pancil:  canvas' ctx 
//x: top left x location of the plot
//y: top left y location of the plot
//width: the width of the plot
//height: the height of the plot
//data: multi-dimensional data
//axis_name: the name of each dimension
function DensityParallelCoordinate(pencil, x, y, width, height, data, axis_name){
    var obj = {},
        version = 'canvas',
        //column, and row
        column = data.length,
        row    = data[0].length,
        //flag for mouse event
        mousepress = false;
        //
        filter_data = []
        //padding of four corner
        axisTopPadding = 20,
        axisBottomPadding = 20,
        axisLeftPadding = 50,
        axisRightPadding = 20,
        //padding on the axis
        padding = 10,
        bincount = 100,
        //the maximum width of density
        density_max_width = 25;
        //gap between axis
        axispadding = (width - axisLeftPadding - axisRightPadding)/column,
        //four corner of plot
        plotx1 = x + axisLeftPadding,
        plotx2 = x + width - axisRightPadding,
        ploty1 = y + axisTopPadding,
        ploty2 = y + height - axisBottomPadding,
        //filter rect
        filters = [],
        //ticket
        ticks = 5,
        //scale of each axis
        scales = [];
    
    for(let i = 0; i < column; i++){
        //scale  of each axis
        let scale = d3.scaleLinear().domain(d3.extent(data[i], function(d){
            return d;
        })).range([ploty2 -  padding, ploty1 + padding]);
        scales.push(scale)

        //filter rect of each axis
        let filter = FilterRect(pencil, x + i * axispadding - density_max_width + axisLeftPadding, ploty1, 2 * density_max_width, ploty2-ploty1);
        filters.push(filter)
        filter_data.push([]);
    }

    obj.draw = function(){
        draw();
    };

    obj.mousedown = function(event){
        mousepress = true;
        for(let i = 0; i < column; i++){
            filters[i].mousedown(event);
        }
        draw();
    };

    obj.mousemove = function(event){
        if(!mousepress)return;
        for(let i = 0; i < column; i++){
            filters[i].mousemove(event);
        }
        fillFilterData();
        draw();
    }

    obj.mouseup = function(event){
        mousepress = false;
        for(let i = 0; i < column; i++){
            filters[i].mouseup(event);
        }
        fillFilterData();
        draw();
    }

    function draw(){
        //clearn current panel
        pencil.clearRect(x, y, width, height);

        //draw lines
        drawLine(data, 'rgba(200, 200, 200, 0.1)');
        if(filter_data[0].length >0)
            drawLine(filter_data, 'rgba(204, 51, 0, 0.2)');

        //draw parallel coordinate axis
        for(let i = 0; i < data.length; i++){
            drawAxis(data[i], x + i * axispadding + axisLeftPadding, ploty1, ticks, axis_name[i]);
            [binMax,binMin] = drawAxisDensityCurve(data[i], x + i * axispadding + axisLeftPadding, ploty1, d3.max(data[i]), d3.min(data[i]), 'gray');
            if(filter_data[0].length >0)
                drawAxisDensityCurve(filter_data[i], x + i * axispadding + axisLeftPadding, ploty1, d3.max(data[i]), d3.min(data[i]), 'rgb(204, 51, 0)', binMax, binMin);
            filters[i].draw();
        }
    }

    function drawAxis(data, x, y, ticks, name){
        let yscale = d3.scaleLinear().domain([ticks, 0]).range([ploty1 + padding, ploty2 - padding]),
            [min, max] = d3.extent(data); 

        //draw veritical line
        pencil.fillStyle = 'black';
        pencil.textAlign = 'center';

        //draw axis name
        pencil.fillText(name, x, y - padding);
        pencil.beginPath();
        pencil.moveTo(x, y);
        pencil.lineTo(x, ploty2);
        pencil.stroke();
        
        //draw ticks
        for(let i = 0; i < ticks; i++)
             pencil.fillText((min + (max-min)/ticks * i).toFixed(2) + '', x - 3 * padding, yscale(i));
    }

    function drawAxisDensityCurve(sampledata, x, y, max, min, color, binmax=-1, binmin = -1){
        //draw axis histogram density curve
        let binScaleStep = (max - min)/bincount,
            bins     = [];
        
        for(let i = 0; i < bincount; i++){
            bins.push(0);
        }

        sampledata.forEach(function(d){
            for(let i = 0; i < bincount; i++){
                if(d >= min + i * binScaleStep && d < min  + (i + 1) * binScaleStep){
                    bins[i] += 1;
                    break;
                }
            }
            if(d == max)
                bins[bincount-1]+=1;
        });

        let binscale = 0,
            step = (ploty2 - ploty1 - 2 * padding)/bincount;
        if(binmax == -1){
            binscale = d3.scaleLinear().domain(d3.extent(bins)).range([0, density_max_width]);
        }else{
            binscale = d3.scaleLinear().domain([binmax, binmin]).range([0, density_max_width]);
        }
        
        
        //left
        pencil.fillStyle = color;
        pencil.beginPath();
        pencil.moveTo(x, ploty2 - padding);
        for(let i = 0; i < bincount; i++){
            pencil.lineTo(x - binscale(bins[i]) , ploty2  - i * step  - padding);
        }
        pencil.lineTo(x, ploty1 + padding);
        pencil.fill();

        //right
        pencil.beginPath();
        pencil.moveTo(x, ploty2 - padding);
        for(let i = 0; i < bincount; i++){
            pencil.lineTo(x + binscale(bins[i]) , ploty2  - i * step  - padding);
        }
        pencil.lineTo(x, ploty1 + padding);
        pencil.fill();
        pencil.closePath();

        return d3.extent(bins);

    }

    function drawLine(sampledata, color){
        pencil.strokeStyle = color;
        for(var i = 0, l = sampledata[0].length; i < l; i++){
            pencil.beginPath();
            pencil.moveTo(x + axisLeftPadding, scales[0](sampledata[0][i]));
            for(var j = 1; j < scales.length; j++)
                pencil.lineTo(x + axispadding * j + axisLeftPadding, scales[j](sampledata[j][i]));
            pencil.stroke();
        }
        pencil.strokeStyle = 'rgba(0, 0, 0, 1)';
    }
   
    function fillFilterData(){
        filter_data = [];
        for(let i=0; i < data.length; i++){
            filter_data.push([]);
        }

        let flag = true;
        for(let i = 0; i < data[0].length; i++){
            flag = true;
            for(let j = 0; j < data.length; j++){
                if(!filters[j].contain(scales[j](data[j][i]))){
                    flag = false;
                    break;
                }
            }

            if(flag){
                for(let j = 0; j < data.length; j++){
                    filter_data[j].push(data[j][i]);
                }
            }
        }
    }
    return obj;
}


function FilterRect(pencil, x, y, width, height){
    var obj = {},
        x1 = x,
        y1 = y,
        y2 = y;//y axis of filter rect

    obj.draw = function(){
        draw();
    };

    obj.contain = function(loc_y){
        return contain(loc_y);       
    }

    obj.mousedown = function(event){
        let eventx = event.pageX - canvas.offsetLeft,
            eventy = event.pageY - canvas.offsetTop;

        //the event happend outside of the axis
        if(!inTheAxis(eventx, eventy))
            return;   
        y1 = eventy;
    };

    obj.mousemove = function(event){
        let eventx = event.pageX - canvas.offsetLeft,
            eventy = event.pageY - canvas.offsetTop;
        
        //the event happend outside of the axis
        if(!inTheAxis(eventx, eventy))
            return;   
        y2 = eventy;
        
    };

    obj.mouseup = function(event){
        let eventx = event.pageX - canvas.offsetLeft,
            eventy = event.pageY - canvas.offsetTop;

        //the event happend outside of the axis
        if(!inTheAxis(eventx, eventy))
            return; 
        y2 = eventy;
    };

    function draw(){
        if(y1 == y2)
            return

        pencil.fillStyle = 'rgba(200, 200, 200, 0.6)';
        pencil.fillRect(x, y1, width, y2 - y1);
        pencil.fillStyle = 'rgba(0, 0, 0, 1)';
    }

    //whether the point in the filter box
    function contain(loc_y){
        if(y1==y2)
            return true;//filter is not active
        else if(y1 < y2)
            return loc_y > y1 && loc_y < y2 ? true : false; 
        else if(y2 < y1)   
            return loc_y > y2 && loc_y < y1 ? true : false; 
    }

    //whether the point in the  axis box
    function inTheAxis(loc_x, loc_y){
        return loc_x > x && loc_x < x + width && loc_y > y && loc_y < y + height? true : false;    
    }

    return obj;
}