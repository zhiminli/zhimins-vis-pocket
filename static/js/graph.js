//  matrixVis is a object represents a function in  program
//  it need information of function
function GraphVis(name, parentId, id, x, y, data, callback=null){
    var obj = {},
    //the rect bottom x, y
    bottom_x = 0,
    bottom_y = 0,
    //the operation that this matrix may perform
    operation =[name,'reorder','clean'],
    //dependency matrix
    matrix = data.matrix,
    //graph data
    graph = getGraphData(),
    //different view
    view = 'matrix-link',
    //is the dependecy matrix merge
    isMerge = false,
    isReorder = false,
    //dependency data
    dependency = data.dependency,
    //key of the matrix
    keys = data.keys,
    //lable name of each node
    labels = data.label,
    //keys = leafOrder(),
    //highlight keys
    highlightKey = [],
    //the width and height of a cell in the matrix
    w = h = 15,
    menu_h = 25,
    menu_w = 50,
    //source code of this function
    source = data.source,

    //color for mouseover, click
    color = {'mouseover':'#01a817', 'clicked':'rgb(189, 7, 7)', 'normal':'white'},
    //line function
    line = d3.line().defined(function(d){return d;})
                .x(function(d){return d[0];})
                .y(function(d){return d[1];}),
    //location of rect location
    loc = {},
    //the dependency link between rect
    link = [],
    //element that is polluted by the error
    graph_highlight = [],
    matrix_highlight = [],
    link_matrix_hightlight = [],
    //current step of the program excutation
    currentstep = 0,
    //drawing handle
    g    = d3.select('#'+parentId).append('g');

    bottom_x = x + keys.length * w + 2 + w + w/2;
    bottom_y = y + keys.length * h + 2 + w;

    //binding the event
    BindingEvent();

    function draw(labels){

        let keys = matrix_Reorder_algorithm();
        //clean
        g.html('');
        if(!isMerge){
            //set up link and rect's location
            loc = {};
            link= [];
            for(let i = 0; i < keys.length; i++)
                loc[keys[i]] = [x + i * w + 2 + w + w/2, y + i * h + 2 + w];

            for(let i= 0; i < keys.length; i++){
                key = keys[i];
                for(let j = 0; j < dependency[key].length; j++){
                    d = dependency[key][j]
                    if(loc[key][1] < loc[d][1])
                        link.push([key, [loc[key], [loc[d][0] - w/2, loc[key][1]], [loc[d][0] - w/2, loc[d][1]-w/2]], d]);
                }
            }
            if(view == 'matrix-link'){
                drawLinkMatrix(keys, labels);
            }
            else if(view == 'matrix'){
                drawMatrix(100, 100, keys, labels);
            }
            else if(view == 'force-layout'){
                drawforceLayout(labels);
            }
        }
        else
            drawMergeMatrix();
    }

    function drawLinkMatrix(keys, labels){
        //draw menu        
        let rects = g.selectAll('.' + id+ 'rect').data(function(){
                let datas = [];
                keys.forEach(function(d){
                    datas.push(labels[d]);
                });
                return datas;
            })
            .enter()
            .append('rect')
            .style('fill', 'white')
            .style('stroke-width', 1)
            .style('stroke', 'gray')
            .style('stroke-opacity', 0.2)
            .attr('x', function(d, i){
                return x + i * w - w * 3;
            })
            .attr('y', function(d, i){
                return y + i * h + h/2 + 3;
            })
            .attr('width', w * 4.5)
            .attr('height', 12);

        let texts = g.selectAll('.' + id+ 'text').data(function(){
                let datas = [];
                keys.forEach(function(d){
                    datas.push(labels[d]);
                });
                return datas;
            })
            .enter()
            .append('text')
            .text(function(d){
                return d;
            })
            .style('fill', 'black')
            .attr('font-size', 10)
            .attr('x', function(d, i){
                return x + i * w - w  ;
            })
            .attr('y', function(d, i){
                return y + i * h +4 + h ;
            })
            .attr("text-anchor", "middle")
            .style('pointer-events', 'none');

        let paths = g.selectAll('.' + id+ 'path').data(link).enter()
            .append('path')
            .attr('class', 'line')
            .attr('d', function(d){
                return line(d[1]);
            })
            .attr("stroke", 'gray')
            .attr("stroke-width", "4")
            .attr("fill", "none")
            .attr('stroke-opacity', 0.1);
        g.selectAll('.' + id+ 'dot').data(link).enter()
            .append('circle')
            .attr('r', 6)
            .attr('cx', function(d){
                return d[1][1][0];
            })
            .attr('cy', function(d){
                return d[1][1][1];
            })
            .style('fill', 'gray')
            .style('opacity', function(d){
                if(highlightKey.length > 0)
                    return highlightKey.indexOf(d[0]) > -1? 1:0;
                else
                    return 1;
            });
        let dots = g.selectAll('.' + id+ 'dot ').data(link).enter()
            .append('circle')
            .attr('r', 4)
            .attr('cx', function(d){
                return d[1][1][0];
            })
            .attr('cy', function(d){
                return d[1][1][1];
            })
            .style('fill', function(d){
                return 'steelblue';
            });
        
        rects.on('mouseover', function(ds, i){
            if(link_matrix_hightlight.indexOf(ds) > -1)
                return;

            dots.style('fill', function(d, j){

                if(link_matrix_hightlight.indexOf(labels[d[0]]) > -1 || link_matrix_hightlight.indexOf(labels[d[2]]) > -1)
                    return color.clicked;

                if(ds == labels[d[0]]  || ds == labels[d[2]])
                    return color.mouseover;
                else
                    return 'steelblue';
            });

            paths.style('stroke', function(d, i){
                if(link_matrix_hightlight.indexOf(labels[d[0]]) > -1 || link_matrix_hightlight.indexOf(labels[d[2]]) > -1)
                    return color.clicked;

                if(ds == labels[d[0]] || ds == labels[d[2]])
                    return color.mouseover;
                else
                    return 'gray';
            }).style('stroke-opacity', function(d, i){
                if(link_matrix_hightlight.indexOf(labels[d[0]]) > -1 || link_matrix_hightlight.indexOf(labels[d[2]]) > -1)
                    return 1;

                if(ds == labels[d[0]] || ds == labels[d[2]])
                    return 1;
                else
                    return 0.1;
            });

            d3.select(this).style('fill', color.mouseover);
        })
        .on('mouseout', function(ds, i){
            if(link_matrix_hightlight.indexOf(ds) > -1)
                return;
            d3.select(this).style('fill', color.normal);

            dots.style('fill', function(d, j){
                return (link_matrix_hightlight.indexOf(labels[d[0]]) > -1 || link_matrix_hightlight.indexOf(labels[d[2]]) > -1) ? color.clicked : 'steelblue';
            });

            paths.style('stroke', function(d, i){
                return (link_matrix_hightlight.indexOf(labels[d[0]]) > -1 || link_matrix_hightlight.indexOf(labels[d[2]]) > -1) ? color.clicked : 'gray';
            }).style('stroke-opacity', function(d, i){
                return (link_matrix_hightlight.indexOf(labels[d[0]]) > -1 || link_matrix_hightlight.indexOf(labels[d[2]]) > -1) ? 1 : 0.1;
            });
        })
        .on('click', function(ds, i){
            if(link_matrix_hightlight.indexOf(ds) == -1){
                link_matrix_hightlight.push(ds);
                d3.select(this).style('fill', color.clicked);
            }else{
                link_matrix_hightlight.splice(link_matrix_hightlight.indexOf(ds), 1);
                d3.select(this).style('fill', color.normal);
            }

            dots.style('fill', function(d, j){
                return (link_matrix_hightlight.indexOf(labels[d[0]]) > -1 || link_matrix_hightlight.indexOf(labels[d[2]]) > -1) ? color.clicked : 'steelblue';
            });

            paths.style('stroke', function(d, i){
                return (link_matrix_hightlight.indexOf(labels[d[0]]) > -1 || link_matrix_hightlight.indexOf(labels[d[2]]) > -1) ?  color.clicked : 'gray';
            }).style('stroke-opacity', function(d, i){
                return (link_matrix_hightlight.indexOf(labels[d[0]]) > -1 || link_matrix_hightlight.indexOf(labels[d[2]]) > -1) ?  1 : 0.1;
            });
        });

        dots.on('mouseover', function(d, i){

        })
    }

    function drawMergeMatrix(){
        g.selectAll().data([name]).enter().append('rect')
        .attr('x', function(d, i){
            return x + i*menu_w;
        }).attr('y', function(d, i){
            return y - menu_h;
        })
        .classed('rect_normal', true)
        .attr('width', 60)
        .attr('height', 60)
        .on('click', function(d){
            isMerge = false;
            draw(keys);
        })
        .call(d3.drag()
                .on('drag', function(d){
                    x = d3.event.x;
                    y = d3.event.y;
                    draw(keys);
        }));

        g.selectAll().data([name]).enter()
        .append('text')
        .attr('x', function(d, i){
            return x  + 60/2;
        })
        .attr('y', y)
        .text(function(d){
            return d;
        })
        .style('fill', 'white')
        .attr("text-anchor", "middle")
        .attr('font-size', 14)
        .attr('alignment-baseline',"central")
        .style('pointer-events', 'none');
    }

    function drawMatrix(x, y, neworder, labels){ 

        //clean the data bucket
        matrix_highlight.splice(0,matrix_highlight.length);
        
        //let neworder = leafOrder();
        let circles = g.selectAll('.'+id+'matrixrect').data(matrix).enter().append('circle')
            .attr('r', w/2-1)
            .attr('cx', function(d, i){
                return x + neworder.indexOf(i % keys.length) * w;
            })
            .attr('cy', function(d, i){
                return neworder.indexOf(Math.floor(i/keys.length)) * h + y;
            })
            .style('fill', function(d, i){
                let c = neworder.indexOf(i % keys.length),
                    r = neworder.indexOf(Math.floor(i/keys.length));
                if(r >= c)
                    return 'white';
                return d == 1?'steelblue':'white';
            })
            .classed('rect_normal', function(d, i){
                let c = neworder.indexOf(i % keys.length),
                    r = neworder.indexOf(Math.floor(i/keys.length));   
                return r >= c ? false: true;
            });
        
        //back ground rectangle
        /*let backgroundRectangle = g.selectAll('.' + id + 'backgroundRect').data(neworder).enter()
                                    .append('rect')
                                    .attr('x', function(d, i){
                                        return x + i * w - w
                                    })
                                    .attr('y', function(d, i){
                                        return y;
                                    })
                                    .attr('height', function(){
                                        return neworder.length * w;
                                    })
                                    .attr('width', function(){
                                        return w;
                                    })
                                    .style('fill', 'gray')
                                    .style('fill-opacity', 0.2);*/

        let rects = g.selectAll('.'+id+'rect').data(neworder).enter()
            .append('rect')
            .attr('x', function(d, i){
                return x + i * w - w*4;
            })
            .attr('y', function(d, i){
                return y + i * h - h/2;
            })
            .attr('width', w*4.5)
            .attr('height', 12)
            .style('fill', 'white')
            .style('stroke', 'gray')
            .style('stroke-opacity', 0.2)
            .style('stroke-width', 2)
            .on('mouseover', function(d, i){
                if(matrix_highlight.indexOf(i) > -1)
                    return;

                circles.style('stroke', function(d, j){
                    let c = neworder.indexOf(j % keys.length),
                        r = neworder.indexOf(Math.floor(j/keys.length));
                    if(r < c){
                        if(matrix_highlight.indexOf(c) > -1 || matrix_highlight.indexOf(r) > -1)
                            return d3.select(this).style('stroke', color.clicked).style('stroke-width', 2);
                        else if(i == c || i == r)
                            return d3.select(this).style('stroke', color.mouseover).style('stroke-width', 2);
                        else
                            return d3.select(this).style('stroke', 'gray').style('stroke-width', 1);
                    }
                });
                d3.select(this).style('fill', color.mouseover);
            })
            .on('mouseout', function(d, i){
                if(matrix_highlight.indexOf(i) > -1)
                    return;
                
                circles.style('stroke', function(d, j){
                    let c = neworder.indexOf(j % keys.length),
                        r = neworder.indexOf(Math.floor(j/keys.length));
                    if(r < c){
                        if(matrix_highlight.indexOf(c) > -1 || matrix_highlight.indexOf(r) > -1)
                            return d3.select(this).style('stroke', color.clicked).style('stroke-width', 2);
                        else
                            return d3.select(this).style('stroke', 'gray').style('stroke-width', 1);
                    }
                });    
                d3.select(this).style('fill', color.normal);
            })
            .on('click', function(d, i){
                if(matrix_highlight.indexOf(i) > -1){
                    matrix_highlight.splice(matrix_highlight.indexOf(i), 1);
                }
                else{
                    matrix_highlight.push(i);
                }

                circles.style('stroke', function(d, j){
                    let c = neworder.indexOf(j % keys.length),
                        r = neworder.indexOf(Math.floor(j/keys.length));
                    if(r < c){
                        if(matrix_highlight.indexOf(c) > -1 || matrix_highlight.indexOf(r) > -1)
                            return d3.select(this).style('stroke', color.clicked).style('stroke-width', 2);
                        else
                            return d3.select(this).style('stroke', 'gray').style('stroke-width', 1);
                    }
                });

                d3.select(this).style('fill', color.clicked);
            });
        
        /*circles.on('mouseover', function(d, i){
            let c = neworder.indexOf(i % keys.length),
                r = neworder.indexOf(Math.floor(i/keys.length));   
            
            circles.style('stroke', function(d, j){
                let c1 = neworder.indexOf(j % keys.length),
                    r1 = neworder.indexOf(Math.floor(j/keys.length));
                if(r1 >= c1) return;
                if(matrix_highlight.indexOf('row'+r1) > -1 || matrix_highlight.indexOf('col'+c1) > -1)
                    return color.clicked;

                if(c == c1 || r == r1)
                    return color.mouseover;
                else
                    return 'gray';
            }).style('stroke-width', function(d, j){
                let c1 = neworder.indexOf(j % keys.length),
                    r1 = neworder.indexOf(Math.floor(j/keys.length));
                if(r1 >= c1) return;
                if(matrix_highlight.indexOf('row'+r1) > -1 || matrix_highlight.indexOf('col'+c1) > -1)
                    return 2;

                if(c == c1 || r == r1)
                    return 2;
                else 
                    return 1;
            });

            rects.style('fill', function(d, i){
                if(matrix_highlight.indexOf('col'+i) > -1  || matrix_highlight.indexOf('row'+i) > -1)
                    return color.clicked;
                else if(c == i || r == i)
                    return color.mouseover;
                else
                    return color.normal;
            });


        }).on('mouseout', function(d, i){
            let c = neworder.indexOf(i % keys.length),
                r = neworder.indexOf(Math.floor(i/keys.length));   
        
            circles.style('stroke', function(d, j){
                let c1 = neworder.indexOf(j % keys.length),
                    r1 = neworder.indexOf(Math.floor(j/keys.length));
                if(r1 >= c1) return;
                if(matrix_highlight.indexOf('row'+r1) > -1 || matrix_highlight.indexOf('col'+c1) > -1)
                    return color.clicked;
                else
                    return 'gray';
            }).style('stroke-width', function(d, j){
                let c1 = neworder.indexOf(j % keys.length),
                    r1 = neworder.indexOf(Math.floor(j/keys.length));
                if(r1 >= c1) return;
                if(matrix_highlight.indexOf('row'+r1) > -1 || matrix_highlight.indexOf('col'+c1) > -1)
                    return 2;
                else 
                    return 1;
            });
            
            rects.style('fill', function(d, i){
                if(matrix_highlight.indexOf('col'+i) > -1  || matrix_highlight.indexOf('row'+i) > -1)
                    return color.clicked;
                else 
                    return color.normal;
            });
        }).on('click', function(d, i){
            let c = neworder.indexOf(i % keys.length),
                r = neworder.indexOf(Math.floor(i/keys.length));   
        
            if(matrix_highlight.indexOf('col'+c) > -1 && matrix_highlight.indexOf('row'+r) > -1){
                matrix_highlight.splice(matrix_highlight.indexOf('col'+c), 1);
                matrix_highlight.splice(matrix_highlight.indexOf('row'+r), 1);   
            }
            else{ 
               matrix_highlight.push('row'+r);
               matrix_highlight.push('col'+c);
            }
            rects.style('fill', function(d, i){
                if(matrix_highlight.indexOf('col'+i) > -1  || matrix_highlight.indexOf('row'+i) > -1)
                    return color.clicked;
                else     
                    return color.normal;
            });            
            check_node_should_be_highlight(circles, neworder, i);
        });*/

        g.selectAll('.' + id+ 'text').data(neworder).enter()
            .append('text')
            .text(function(d, i){
                return labels[d];
            })
            .attr('x', function(d, i){
                return x + i * w -2 * w;
            })
            .attr('y', function(d, i){
                return y + i * h - 2;
            })
            .attr("text-anchor", "middle")
            .attr('font-size', 12)
            .attr('alignment-baseline',"central")
            .style('pointer-events', 'none');  
    }

    function check_node_should_be_highlight(circles, neworder, i){
        circles.style('stroke', function(d, j){
            let c1 = neworder.indexOf(j % keys.length),
                r1 = neworder.indexOf(Math.floor(j / keys.length));
            if(r1 >= c1) return;

            if(matrix_highlight.indexOf('col'+c1) > -1 || matrix_highlight.indexOf('row'+r1) > -1)
                return color.clicked;
            else
                return 'gray';
        }).style('stroke-width', function(d, j){
            let c1 = neworder.indexOf(j % keys.length),
                r1 = neworder.indexOf(Math.floor(j/keys.length));
            if(r1 >= c1) return;

            if(matrix_highlight.indexOf('col'+c1) > -1 || matrix_highlight.indexOf('row'+r1) > -1)
                return 2;
            else 
                return 1;
        });
    }

    function drawforceLayout(labels){
        let width = 800,
            height = 700;
        //clean high light element array
        graph_highlight.splice(0,graph_highlight.length);

        let simulation = d3.forceSimulation()
            .force('link', d3.forceLink().id(function(d){return d.id;}))
            .force('charge', d3.forceManyBody())
            .force('center', d3.forceCenter(width*2/3, height*2/3));
        
        let link = g.selectAll('line')
                    .data(graph.links)
                    .enter()
                    .append('line')
                    .style('stroke-width', 2)
                    .style('stroke', '#999');
        
        let node = g.classed('nodes', true).selectAll('.nodelinecircle')
                    .data(graph.nodes)
                    .enter()
                    .append('g') 
                    .call(d3.drag()
                        .on('start', dragstarted)
                        .on('drag', dragged)
                        .on('end', dragended));

        let circles = node.append('circle')
                    .attr('class', 'force_layout_node')
                    .attr('r', 8)
                    .on('mouseover', function(d){
                        //this node is already been highlighted
                        if(graph_highlight.indexOf(d) > -1)
                            return;
                        else{
                            link.style('stroke-width', function(l) {
                                if(check_Line_Incident_To_highlight_node(l))
                                    return 4;
                                else
                                    return d === l.source || d === l.target? 4: 2;
                            }).style('stroke', function(l){
                                if(check_Line_Incident_To_highlight_node(l))
                                    return color.clicked;
                                else
                                    return d === l.source || d === l.target?color.mouseover:'#999';
                            });
                            d3.select(this).style('fill', color.mouseover);
                        }
                    })
                    .on('mouseout', function(d){
                        //this node is already been highlighted
                        if(graph_highlight.indexOf(d) > -1)
                            return;
                        else{
                            link.style('stroke-width', function(l){
                                if(check_Line_Incident_To_highlight_node(l))
                                    return 4;
                                else 
                                    return 2;
                            });
                            link.style('stroke', function(l){
                                if(check_Line_Incident_To_highlight_node(l))
                                    return 'rgb(189, 7, 7)';
                                else 
                                    return '#999';
                            });
                            d3.select(this).style('fill', 'white');
                        }                        
                    })
                    .on('click', function(d){
                        //high light the node and its incident
                        let index = graph_highlight.indexOf(d);
                        if(index == -1){
                            //add
                            graph_highlight.push(d);
                            d3.select(this).style('fill', 'rgb(189, 7, 7)');
                        }else{
                            //remove
                            graph_highlight.splice(index, 1);
                            d3.select(this).style('fill', 'white');
                        }
                            
                        link.style('stroke-width', function(l) {
                            if (check_Line_Incident_To_highlight_node(l))
                                return 4;
                            else
                                return 2;
                        }).style('stroke', function(l){
                            if (check_Line_Incident_To_highlight_node(l))
                                return 'rgb(189, 7, 7)';
                            else 
                                return '#999';
                        });
                        
                    });

        node.append('text')
            .text(function(d){
                return labels[d.id];
            })
            .attr("text-anchor", "middle")
            .attr('font-size', 10)
            .style('pointer-events', 'none')
            .attr('alignment-baseline',"central");
            
            
        link.on('mouseover', function(d){
            circles.style('fill', function(l){
                if(graph_highlight.indexOf(l) > -1){
                    return color.clicked;
                }else{
                    return (d.source == l || d.target == l) ? color.mouseover : color.normal;
                }  
            });

            if(check_Line_Incident_To_highlight_node(d))
                return;
            d3.select(this).style('stroke', color.mouseover);
            d3.select(this).style('stroke-width', 4);
        }).on('mouseout', function(d){
            circles.style('fill', function(l){
                if(graph_highlight.indexOf(l) > -1){
                    return color.clicked;
                }else{
                    return color.normal
                }
            });
            if(check_Line_Incident_To_highlight_node(d))
                return;
            d3.select(this).style('stroke', '#999');
            d3.select(this).attr('stroke-width', 2);
        });
                   
                     
        simulation.nodes(graph.nodes).on("tick", ticked);
        simulation.force("link").links(graph.links);

        function ticked() {
            link
                .attr("x1", function(d) { return d.source.x; })
                .attr("y1", function(d) { return d.source.y; })
                .attr("x2", function(d) { return d.target.x; })
                .attr("y2", function(d) { return d.target.y; });

            node.attr('transform', function(d){return 'translate(' + d.x + "," + d.y + ")"});
        }

        function dragstarted(d) {
            if (!d3.event.active) 
                simulation.alphaTarget(0.3).restart();
            d.fx = d.x;
            d.fy = d.y;
        }

        function dragged(d) {
            d.fx = d3.event.x;
            d.fy = d3.event.y;
        }

        function dragended(d) {
            if (!d3.event.active) 
                simulation.alphaTarget(0);
            d.fx = null;
            d.fy = null;
        }

    }

    function check_Line_Incident_To_highlight_node(l){
        return (graph_highlight.indexOf(l.source) > -1 || graph_highlight.indexOf(l.target) > -1)
    }

    function getGraphData(){
        let nodes = [],
            links = [];
        
        Object.keys(data.dependency).forEach(function(element) {
            nodes.push({'id':element});
            data.dependency[element].forEach(function(d){
                links.push({'source':element, 'target':d});
            })
        }, this);

        return {'nodes':nodes, 'links':links};
    }

    //pass the reorder option and return the new order of the matrix
    function matrix_Reorder_algorithm(){
        let option = $('#matrix_reorder_option').val();
        let mat = [],
            col = row = Math.sqrt(matrix.length);
        
        for(let i = 0; i < row; i++){
            let entry = [];
            for(let j = 0; j < col; j++){
                entry.push(matrix[j + i * row]);
            }
            mat.push(entry);
        }

        let graph = reorder.mat2graph(mat, true);

        if(option == 'Default')
            return keys;
        else if(option == 'Optimal-Leaf-Order')
            return reorder.optimal_leaf_order()(mat);
        else if(option == 'Cuthill-McKee')
            return reorder.cuthill_mckee_order(graph);
        else if(option == 'Reverse-Cuthill-Mckee-Order')
            return reorder.reverse_cuthill_mckee_order(graph);
        else if(option == 'DFS_Max')
            return DFS_Max_Sort();
    }

    function nodeDegreeOrder(){
        let nodes_degree = new Array(keys.length).fill(0),
            nodes_degree_index = [],
            order = [];

        for(let i = 0; i < matrix.length; i++){
            nodes_degree[Math.floor(i/keys.length)] += matrix[i];
        }

        for(let i = 0; i < nodes_degree.length; i++){
            nodes_degree_index.push([i, nodes_degree[i]]);
        }

        nodes_degree_index.sort(function(node1, node2){
            return node1[1] < node2[1] ? -1 : 1;
        });

        for(let i = 0; i < nodes_degree_index.length; i++){
            order.push(nodes_degree_index[i][0]);
        }

        return order;
    }

    function DFS_Sort(){
        let keys = Object.keys(dependency),
        orderqueue = [];
        keys.sort().reverse();
        
        while(keys.length > 0){
            currentNode = parseInt(keys.pop());
            if(orderqueue.indexOf(currentNode) == -1)
                dependecy_DFS_tracking(currentNode, dependency, orderqueue);
        }
        return orderqueue;
    }

    //path tracking DFS
    function dependecy_DFS_tracking(node, dependency, orderqueue){
        orderqueue.push(node);
        if(dependency[node].length > 0){
            for(let i = 0; i < dependency[node].length; i++){
                if(orderqueue.indexOf(dependency[node][i])!=-1 || node > dependency[node][i])
                    continue;
                dependecy_DFS_tracking(dependency[node][i], dependency, orderqueue);
            }
        }
    };

    //depth first search with each time visit the maximum dependency node
    function DFS_Max_Sort(){
        let keys = nodeDegreeOrder(),
            orderqueue = [];

        while(keys.length > 0){
            currentNode = parseInt(keys.shift());
            if(orderqueue.indexOf(currentNode) == -1)
                dependency_DFS_Max_tracking(currentNode, dependency, orderqueue);
        }
        return orderqueue;
    }


    function dependency_DFS_Max_tracking(node, dependency, orderqueue){
        orderqueue.push(node);
        if(dependency[node].length > 0){
            let nodes = order_Connected_Node_Based_ON_DegreeOfConnectivity(dependency[node]);//find connected node with maximum degree
            for(let i = 0; i < nodes.length; i++){
                if(orderqueue.indexOf(nodes[i]) != -1)//node > nodes[i] means loop and will stop here
                    continue;
                dependency_DFS_Max_tracking(nodes[i], dependency, orderqueue);
            }
        }
    }

    function order_Connected_Node_Based_ON_DegreeOfConnectivity(connectedNodes){
        let nodes_degree = new Array(connectedNodes.length).fill(0),
            nodes_degree_index = [],
            order = [];

        for(let i = 0; i < connectedNodes.length; i++){
            for( let j = 0; j < keys.length; j++)
                nodes_degree[i] += matrix[connectedNodes[i]*keys.length+j];
        }

        for(let i = 0; i < nodes_degree.length; i++){
            nodes_degree_index.push([connectedNodes[i], nodes_degree[i]]);
        }

        nodes_degree_index.sort(function(node1, node2){
            return node1[1] < node2[1] ? -1 : 1;
        });

        for(let i = 0; i < nodes_degree_index.length; i++){
            order.push(nodes_degree_index[i][0]);
        }

        return order;
    }


    function dependency_BFS(node, dependency, queue, container){
        queue.push(node);
        let currentNode = null;

        while(queue.length > 0){
            currentNode = queue.pop();
            container.push(currentNode);
            for(let  i = 0; i < dependency[currentNode].length; i++){
                if(container.indexOf(dependency[currentNode][i]) == -1 && queue.indexOf(dependency[currentNode][i]) == -1)
                    queue.push(dependency[currentNode][i]);
            }
        }
    }

    //event binding
    function BindingEvent(){
        $('#matrix_reorder_option').change(function(){
            draw(labels);
        });

        $('#graph_dataset').change(function(){

        });
    }

    //fetch data from server
    function fetchData(){

    }

    //draw the dependency matrix
    obj.draw = function(){
        draw(labels);
    };

    obj.setView = function(v){
        view = v;
        draw(labels);
    }

    //get bottom x, y
    obj.getRectBottom  = function(){
        return [bottom_x, bottom_y];
    };

    return obj;  
}