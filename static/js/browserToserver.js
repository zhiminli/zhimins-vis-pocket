// file: name of the data set
function load_data(callback, para){
    $.ajax({
        url:'/_getData',
        type:'POST',
        contentType:'application/json',
        data:JSON.stringify(para),
        dataType:'json',
        success:function(para){
            callback(para);
        }
    });
}


// file: name of the data set
function load_file_data(para, callback){
    $.ajax({
        url:'/_getGraphData',
        type:'POST',
        contentType:'application/json',
        data:JSON.stringify(para),
        dataType:'json',
        success:function(para){
            callback(para);
        }
    });
}