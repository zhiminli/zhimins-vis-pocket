

load_data(load_data_handle, getPara())

$('#filter').change(function(){
    load_data(load_data_handle, getPara());
});

$('#cluster').change(function(){
    load_data(load_data_handle, getPara());
});

$('#number').change(function(){
    load_data(load_data_handle, getPara());
});

$('#cube_n').change(function(){
    load_data(load_data_handle, getPara());
});

$('#metrics').change(function(){
    load_data(load_data_handle, getPara());
});

$('#overlap').change(function(){   
    load_data(load_data_handle, getPara());
});

$('#mapperid').change(function(){
    load_data(load_data_handle, getPara());
});

function getPara(){
    var dim        = $('#mapperid').val(),
        filter     = $('#filter').val(),
        clustering = $('#cluster').val(),
        cluster_n  = $('#cluster_num').val(),
        cube_n     = $('#cube_n').val(),
        overlap    = $('#overlap').val();
    
    //var json = {'dim':dim,'filter': filter,'clustering': clustering, 'cluster_n':cluster_n, 'cube_n':cube_n, 'metrics':metrics, 'overlap':overlap};
    var json = {'dim':dim,'filter': filter,'clustering': clustering, 'cluster_n':cluster_n, 'cube_n':cube_n, 'overlap':overlap};
    
    return json
}

//ID is the location of the element histogram will draw on
//Data is a dictionary that come with key and the number of 
//element under each key.
function drawHistogram(id, data){
    var bins  = Object.values(data),
        keys  = Object.keys(data),
        widthscale = d3.scaleLinear().domain(d3.extent(bins)).range([0, 100]),
        height = 30,
        padding =5,
        svg = d3.select('#info').append('svg').attr('width', 200).attr('height', 400);

    svg.selectAll('.digits_rect').append('g').data(bins).enter().append('rect')
        .attr('x', function(d){
            return 20
        })
        .attr('y', function(d, i){
            return 10 + i * height + i * padding;
        })
        .attr('width', function(d){
            return widthscale(d);
        })
        .attr('height', function(d){
            return height;
        })
        .style('fill', 'steelblue');

    svg.selectAll('.digits_text').append('g').data(keys).enter().append('text')
        .attr('x', function(d){
            return 5;
        })
        .attr('y', function(d, i){
            return 10 + i * height + i * padding + height/2;
        })
        .text(function(d){return d+''});
    
    svg.selectAll('.histgram_text').data(bins).enter().append('text')
        .attr('x', function(d){
            return widthscale(d) + 20;
        })
        .attr('y', function(d, i){
            return 10 + i * height + i * padding + height/2;
        })
        .text(function(d){return d+'';});
}

function drawGraph(para){
    //clean canvas
    $('#canvas').empty();

    var width = 400   
        height= 400,
        g     = d3.select('#canvas').attr('width', 1400).attr('height', 900).append('g'),
        graph = {'nodes':[], 'links':[]},
        sizes_scale = d3.scaleLinear().domain(d3.extent(Object.values(para.node), function(d){
            return d.length;
        })).range([3, 10]),
        colorscale = d3.scaleLinear().domain([0, 1]).range(['blue', 'red']);
    
    Object.keys(para.node).forEach(function(d){
        graph.nodes.push({'id':d, 'value':para.node[d]});
    });
    //for(var i = 0; i < Object.keys(para.node).length; i++)
    //    graph.nodes[i] = {'id':i, 'value':para.node[i]};
    
    para.link.forEach(function(d){
        graph.links.push({'source': d[0], 'target':d[1]});
    });
    

    var simulation  = d3.forceSimulation()
        .force('link', d3.forceLink().id(function(d){
            return d.id;
        }))
        .force('charge', d3.forceManyBody())
        .force('center', d3.forceCenter(700, 400));
        

    var links   = g.append("g")
        .attr("class", "links")
        .selectAll("line")
        .data(graph.links)
        .enter().append("line")
        .attr("stroke", "#000")
        .attr("stroke-width", 1.5);

    var nodes   = g.append("g")
        .attr("class", "nodes")
        .selectAll("circle")
        .data(graph.nodes)
        .enter().append("circle")
        .attr("r", function(d){
            return sizes_scale(d.value.length);
        })
        .style('fill', function(d){
            var set = {};
            d.value.forEach(function(val){
                n = para.target[val]
                set.hasOwnProperty(n)?set[n]++:set[n] = 1
            });
            return colorscale(Math.max(...Object.values(set))/d.value.length);
        })
        .on('click', function(d){
            $('#info').empty();
            hist = {}
            for(var i = 0; i < d.value.length; i++){
                key = para.target[d.value[i]]
                hist.hasOwnProperty(key)?hist[key] += 1: hist[key] = 1; 
            }

            // highlight node in the graph
            d3.selectAll('.nodes circle').classed('nodes_highlight', false).classed('nodes', true);
            d3.select(this).classed('nodes', false);
            d3.select(this).classed('nodes_highlight', true);

            //highlight text in the embedding
            var indexs = d.value;
            d3.selectAll('#embedding text').classed('embedding_text_highlight', function(data, i){
               return indexs.indexOf(i) > -1? true:false;
            }).classed('embedding_text', function(data, i){
                return indexs.indexOf(i) > -1? false:true;
            });
            


            drawHistogram($('#info'), hist);
            drawImage(d.value, para.data);
        })
        .call(d3.drag()
                .on("start", dragstarted)
                .on("drag", dragged)
                .on("end", dragended));

    simulation.nodes(graph.nodes).on("tick", ticked);
    simulation.force("link").links(graph.links);

    function ticked() {
            links
                .attr("x1", function(d) { return d.source.x; })
                .attr("y1", function(d) { return d.source.y; })
                .attr("x2", function(d) { return d.target.x; })
                .attr("y2", function(d) { return d.target.y; });
            nodes
                .attr("cx", function(d) { return d.x; })
                .attr("cy", function(d) { return d.y; });
    }
       
    function dragstarted(d) {
        if (!d3.event.active) simulation.alphaTarget(0.3).restart();
        d.fx = d.x;
        d.fy = d.y;
    }

    function dragged(d) {
        d.fx = d3.event.x;
        d.fy = d3.event.y;
    }

    function dragended(d) {
        if (!d3.event.active) simulation.alphaTarget(0);
        d.fx = null;
        d.fy = null;
    }
}

function drawImage(indexs, data){

    var canvas = $('#html_canvas')[0],
        ctx    = canvas.getContext('2d'),
        cell_w = cell_h = 4,
        image_w = image_h = 32;
    
    ctx.clearRect(0, 0, 1400, 800);
    var colorscale = d3.scaleLinear().domain([0, 15]).range([0, 1]);
    for(var i = 0;  i < indexs.length; i++){
        digits = data[indexs[i]];
        digits_x = i % 25 * (image_w +  10) + 100;
        digits_y = Math.floor((i) / 25) * (image_h  + 10);
        for(var j = 0; j < digits.length; j++){
            ctx.fillStyle = "rgba(32, 45, 21,"+ colorscale(digits[j]) +")";
            ctx.fillRect(digits_x + (j) % 8  * cell_w,  digits_y + Math.floor((j) / 8) * cell_h, cell_w, cell_h);
        }
    }
}

function draw_embedding(data, target, cr=3, padding=4){
    if(data[0].length == 1)
        draw_1D_embedding(data, target);
    else
        draw_2D_embedding(data, target);
}

function draw_1D_embedding(data, target, c=3, paddng=4){

}

function draw_2D_embedding(data, target, c=3, padding=4){

    $('#embedding').empty();
    var width = 400,
        height = 400,
        svg = d3.select('#embedding').append('svg').attr('width', width).attr('height', height);

    var xscale = d3.scaleLinear().domain(d3.extent(data, function(d){
        return d[0];
    })).range([padding, width - padding * 2]),
        yscale = d3.scaleLinear().domain(d3.extent(data, function(d){
            return d[1];
    })).range([padding, height - padding * 2]);

    
    svg.selectAll('.point').data(data).enter().append('text')
        .attr('class','embedding_text')
        .attr('x', function(d){
            return xscale(d[0]);
        })
        .attr('y', function(d){
            return yscale(d[1]);
        })
        .text(function(d, i){
            return target[i]+'';
        })
        .attr("font-family", "sans-serif")
        .attr("font-size", "10px")
        .attr('class', 'point')
}

function load_data_handle(para){
    drawGraph(para);
    draw_embedding(para.dim_reducton, para.target);
}